﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour
{
    [Header("Player settings")]
    [SerializeField] private float movementSpeed = 10;
    [SerializeField] private float rotationSpeed = 200;
    [SerializeField] private float turboSpeed = 20;

    [Header("Player data bools")]
    public static bool usingTurbo;
    public static bool isMoving;

    void Start()
    {
        usingTurbo = false;
        isMoving = false;
    }

    void Update()
    {
        InputHandling();

        // restart scene on key pressed
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    /// <summary>
    /// Handle player's input (drive).
    /// </summary>
    private void InputHandling()
    {
        // let player rotate if moving forward / backward
        if (Input.GetButton("Vertical"))
        {
            isMoving = true;
            transform.Rotate(0, Input.GetAxis("Horizontal") * Time.deltaTime * rotationSpeed, 0);
        }
        else
        {
            isMoving = false;
        }

        // if player is using turbo
        if (Input.GetButton("Turbo"))
        {
            usingTurbo = true;
            transform.Translate(0, 0, Input.GetAxis("Vertical") * Time.deltaTime * turboSpeed);
        }
        else
        {
            usingTurbo = false;
            transform.Translate(0, 0, Input.GetAxis("Vertical") * Time.deltaTime * movementSpeed);
        }
    }
}