﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : MonoBehaviour
{
    [Header("Checkpoint settings")]
    public List<GameObject> checkPoints;
    private int checkPointNumber;

    [Header("Field of View")]
    [SerializeField] private float fov = 70;
    [SerializeField] private float fovRange = 20;
    private Vector3 lastKnownPosition;

    [Header("Hear settings")]
    [SerializeField] private float hearRange = 20;
    [SerializeField] private float closeHearRange = 10;

    [Header("Other settings")]
    private GameObject player;
    private NavMeshAgent agent;
    private Vector3 startPosition;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        agent = GetComponent<NavMeshAgent>();

        startPosition = transform.position;

        checkPointNumber = 0;
    }


    /// <summary>
    /// Enemy's idle behaviour.
    /// </summary>
    public void Idle()
    {
        // move back to start position
        if (transform.position != startPosition)
        {
            agent.SetDestination(startPosition);
        }
    }


    /// <summary>
    /// Enemy's patrolling behaviour.
    /// </summary>
    public void Patrol()
    {
        // if agent is stopped, set new destination
        if (agent.isStopped)
        {
            checkPointNumber++;

            // if checkPointNumber is bigger than index of last checkpoint, reset checkPointNumber
            if (checkPointNumber >= checkPoints.Count)
            {
                checkPointNumber = 0;
            }

            // set destination and start moving
            agent.SetDestination(checkPoints[checkPointNumber].transform.position);
            agent.isStopped = false;
        }
        // if agent reached destination, stop moving
        else if (agent.remainingDistance == 0f)
        {
            Debug.Log("Enemy reached checkpoint.");
            agent.isStopped = true;
        }
    }


    /// <summary>
    /// Resumes patrolling by setting back last checkpoint to move to.
    /// </summary>
    public void ResumePatrol()
    {
        if (checkPoints.Count > 0)
        {
            agent.SetDestination(checkPoints[checkPointNumber].transform.position);
        }
    }


    /// <summary>
    /// Returns true when player is visible in enemy's field of view.
    /// </summary>
    public bool CheckFOV()
    {
        Vector3 targetDir = player.transform.position - transform.position;
        float angle = Vector3.Angle(targetDir, transform.forward);

        float distance = Vector3.Distance(player.transform.position, transform.position);

        if (angle < fov / 2f && distance <= fovRange)
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, targetDir, out hit, fovRange))
            {
                // if player is not behind an obstacle, then he is visible
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    //Debug.Log("Player detected.");
                    return true;
                }
            }
        }

        return false;
    }


    /// <summary>
    /// Returns true when player was heard in enemy's hear range.
    /// </summary>
    public bool CheckHearings()
    {
        float distance = Vector3.Distance(player.transform.position, transform.position);

        // if player is using turbo on larger distance
        if (distance <= hearRange && PlayerController.usingTurbo && PlayerController.isMoving)
        {
            Debug.Log("Player heared.");
            return true;
        }
        // if player is just moving near enemy
        if (distance <= closeHearRange && PlayerController.isMoving)
        {
            Debug.Log("Player heared.");
            return true;
        }

        return false;
    }


    /// <summary>
    /// Move towards the player and save his current position as last known position.
    /// </summary>
    public void ChasePlayer()
    {
        agent.SetDestination(player.transform.position);
        lastKnownPosition = player.transform.position;
    }


    /// <summary>
    /// Set player's position as last known position.
    /// </summary>
    public void SetHeardPosition()
    {
        lastKnownPosition = player.transform.position;
    }


    /// <summary>
    /// Returns true when last known position is reached.
    /// </summary>
    public bool CheckLastKnownPosition()
    {
        // move to last known position
        agent.SetDestination(lastKnownPosition);

        // if last known position reached, return true
        if (agent.remainingDistance == 0f)
        {
            Debug.Log("Player lost");
            return true;
        }

        // if last known position is not reached, return false
        return false;
    }


    private void OnDrawGizmosSelected()
    {
        Quaternion leftRayRotation = Quaternion.AngleAxis(-fov / 2f, Vector3.up);
        Quaternion rightRayRotation = Quaternion.AngleAxis(fov / 2f, Vector3.up);

        Vector3 leftRayDirection = leftRayRotation * transform.forward;
        Vector3 rightRayDirection = rightRayRotation * transform.forward;

        // Draw FOV range
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, leftRayDirection * fovRange);
        Gizmos.DrawRay(transform.position, rightRayDirection * fovRange);

        // Draw hear range
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, hearRange);
    }
}
