﻿using UnityEngine;

public class IdleBehaviour : StateMachineBehaviour
{
    private Enemy enemy;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.gameObject.GetComponent<Enemy>();

        // send on patrol if has checkpoints
        if (enemy.checkPoints.Count > 0)
        {
            animator.SetTrigger("shouldPatrol");
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // if player was spotted, follow him
        if (enemy.CheckFOV())
        {
            animator.SetTrigger("playerSpotted");
        }
        // if player was heard, search him
        else if (enemy.CheckHearings())
        {
            animator.SetBool("wasHeard", true);
            animator.SetTrigger("playerHeard");
        }
        // if no checkpoints, keep idling
        else if (enemy.checkPoints.Count <= 0)
        {
            enemy.Idle();
        }
    }
}