﻿using UnityEngine;

public class PatrolBehaviour : StateMachineBehaviour
{
    private Enemy enemy;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.gameObject.GetComponent<Enemy>();

        // reset triggers
        animator.ResetTrigger("playerSpotted");
        animator.ResetTrigger("playerHeard");
        animator.ResetTrigger("playerLost");

        // restart patrol if interrupted
        enemy.ResumePatrol();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // keep patrolling if has checkpoints
        if (enemy.checkPoints.Count > 0)
        {
            enemy.Patrol();
        }
        // go idle otherwise
        else
        {
            animator.SetTrigger("shouldIdle");
            return;
        }

        // if player was spotted, follow him
        if (enemy.CheckFOV())
        {
            animator.SetTrigger("playerSpotted");
        }
        // if player was heard, search him
        else if (enemy.CheckHearings())
        {
            animator.SetBool("wasHeard", true);
            animator.SetTrigger("playerHeard");
        }
    }
}