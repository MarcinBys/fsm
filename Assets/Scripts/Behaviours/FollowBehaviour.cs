﻿using UnityEngine;

public class FollowBehaviour : StateMachineBehaviour
{
    private Enemy enemy;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.gameObject.GetComponent<Enemy>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy.ChasePlayer();

        // if player was lost from sight, search him
        if (!enemy.CheckFOV())
        {
            animator.SetTrigger("playerLost");
        }
    }
}