﻿using UnityEngine;

public class SearchBehaviour : StateMachineBehaviour
{
    private Enemy enemy;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.gameObject.GetComponent<Enemy>();

        if (animator.GetBool("wasHeard"))
        {
            enemy.SetHeardPosition();
            animator.SetBool("wasHeard", false);
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // if player was spotted, follow him
        if (enemy.CheckFOV())
        {
            animator.SetTrigger("playerSpotted");
        }
        // if player was heard, search him
        else if (enemy.CheckHearings())
        {
            animator.SetBool("wasHeard", true);
            animator.SetTrigger("playerHeard");
        }

        // if last known position checked and player was still not found, go on a patrol
        if (enemy.CheckLastKnownPosition())
        {
            animator.SetTrigger("playerLost");
        }
    }
}